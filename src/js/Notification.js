import { formatCurrency } from './utils';
import classNames from 'classnames';
import Card from './Card';

export default class Notification {
  static get types() {
    return {
      PEPPERONI: "pepperoni",
      MARGHERITA: "margherita",
      HAWAIIAN: "hawaiian",
    };
  }

  constructor() {
    this.container = document.createElement("div");
    this.container.classList.add("notification-container");
  }

  render({ price, type }) {
    const formattedPrice = formatCurrency(price);

    const pizzaType = classNames([`type-${type}`]);
    const isDangerClass = classNames({'is-danger': type == Notification.types.HAWAIIAN});
    const template = `
<div class="notification ${pizzaType} ${isDangerClass}">
  <button class="delete"></button>
  🍕 <span class="type">${type}</span> (<span class="price">${formattedPrice}</span>) has been added to your order.
</div>
    `;

    this.container.innerHTML = template;
    this.container.querySelector("button.delete").addEventListener("click", () => {
      this.empty();
      this.removeNotification();
    });
  }

  empty() {
    this.container.innerHTML = "";
  }

  removeNotification() {
    this.container.parentElement.removeChild(this.container);
  }
}